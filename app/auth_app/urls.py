from django.conf.urls import url
from django.contrib.auth.views import LoginView, LogoutView

from auth_app.views import proxy, RegistrationView

urlpatterns = [
    url('registration', RegistrationView.as_view(success_url='ru'), name='registration'),
    url('login', LoginView.as_view(
        template_name='auth_app/login.html',
        success_url='ru'
    ), name='login'),
    url('logout', LogoutView.as_view(next_page='login'), name='logout'),
    url('(?P<path>.*)', proxy),
]
