from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView
from proxy.views import proxy_view


@csrf_exempt
@login_required(login_url='/login')
def proxy(request, path):
    if not path:
        return redirect('/en/')
    remote_url = 'http://172.17.0.1:8000/' + path
    return proxy_view(request, remote_url)


class RegistrationView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'auth_app/registration.html'
    model = User
